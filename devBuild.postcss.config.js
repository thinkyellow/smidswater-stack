let autoprefixer = require('autoprefixer')
module.exports = {
  parser: 'postcss-scss',
  plugins: [
    require('postcss-will-change-transition'),
    require('postcss-calc'),
    require('postcss-css-variables'),
    autoprefixer ({
      browsers: ['>0%'],
      cascade: true,
      grid: true
    })
  ]
}
/* I think you can compile it around 13:30, then i will have the update ok?
yes so I will ask Dennis what to do in the meantime, yeah
 */
