# Smidswater Stack

## Init video
[![asciicast](https://asciinema.org/a/84jgiappu74a3r8w54dj3yvro.png)](https://asciinema.org/a/84jgiappu74a3r8w54dj3yvro)


## Install
```npm install```

## Initialize
```npm run init```

## Bower
### Install embedded packages
```npm run bower```


## Build and Watch
```npm run gulp```

## Build
### Build All
```npm run build```

### Build Javascript Only
```npm run build:js```

### Build SCSS Only
```npm run build:scss```

### Build Images Only
```npm run build:img```

### Copy Fonts
```npm run copy:fonts```

## Watch
### Watch all & Build all
```npm run watch```

### Watch Javascript Only
```npm run watch:js```

### Watch SCSS Only
```npm run watch:scss```

### Watch Images Only
```npm run watch:img```

### Watch Fonts Only
```npm run watch:fonts```


## Update stack
```
git checkout master
git pull origin master
npm install
```