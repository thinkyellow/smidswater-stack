'use strict'
const gulp = require('gulp')
const gulpSass = require('gulp-sass')
const gulpWatch = require('gulp-watch')
const gulpImageMin = require('gulp-imagemin')
const fs = require('fs')

const gulpReplace = require('gulp-replace')
const gulpRename = require('gulp-rename')
const gulpBower = require('gulp-bower')
const gulpIf = require('gulp-if')
const gulpNotify = require('gulp-notify')
const gulpSourceMaps = require('gulp-sourcemaps')
const runSequence = require('run-sequence')
const path = require('path')

const cssstats = require('cssstats')
const gulpPostCSS = require('gulp-postcss')
const autoprefixer = require('autoprefixer')
const postCssWillChange = require('postcss-will-change-transition')
const postCssCalc = require('postcss-calc') //calc is in cssnano
const postCssVariables = require('postcss-css-variables')
const cssnano = require('cssnano')

const pluginsProdBuild = [
    autoprefixer({
        browsers: ['>0%'],
        cascade: true,
        grid: true,
        discardUnused: false,
        discardOverridden: false
    }),
    require('postcss-calc'),
    postCssVariables({
        preserve: false
    }),
    postCssWillChange
    // not working some url issue but not with fontAwesome
    // cssnano({
    //   preset: ['default', {
    //     normalizeUrl: false,
    //     normalizeString: false
    //   }]
    // })
]

const gulpBetterRollup = require('gulp-better-rollup')
const rollupPluginBabel = require('rollup-plugin-babel')
const rollupPluginUglify = require('rollup-plugin-uglify')
const rollupPluginNodeResolve = require('rollup-plugin-node-resolve')
const rollupPluginCommonjs = require('rollup-plugin-commonjs')
const config = {
    sourceMaps: !(process.argv[process.argv.length - 1] === 'production')
}

let src = null

const task = function () {
    let args = Array.from(arguments)
    if (args[0] !== 'load' && src === null) {
        if (Array.isArray(args[1])) {
            args[1].splice(0, 'load')
        } else {
            args.splice(1, 0, ['load'])
        }
    }
    gulp.task.apply(gulp, args)
}

let promise = new Promise((resolve, reject) => {
    fs.readFile('../packages.json', 'utf8', (err, _data) => {
        if (err) {
            fs.readFile('packages.json', 'utf8', (err, _data) => {
                if (err) {
                    resolve('{}')
                } else {
                    resolve(_data)
                }
            })
        } else {
            resolve(_data)
        }
    })
})

let promise2 = new Promise((resolve, reject) => {
    promise.then((data) => {
        gulp.src(['_bower.json'])
            .pipe(gulpReplace('%dependencies%', data))
            .pipe(gulpReplace('%bower_path%', src.bower))
            .pipe(gulpRename('bower.json'))
            .pipe(gulp.dest('.'))
            .on('end', resolve)
        gulp.src(['_bowerrc'])
            .pipe(gulpReplace('%bower_path%', src.bower))
            .pipe(gulpRename('.bowerrc'))
            .pipe(gulp.dest('.'))
            .on('end', resolve)
    })
})

task('load', () => {
    if (src === null) {
        try {
            src = JSON.parse(fs.readFileSync('../gulp.paths.json', 'utf8'))
        } catch (e) {
            src = JSON.parse(fs.readFileSync('gulp.paths.json', 'utf8'))
        }
    }
})
task('production', () => {})
task('build:img', () =>
    gulp.src(src.img, {
        cwd: src.prefix
    })
    .pipe(gulpImageMin())
    .pipe(gulp.dest(src.public + 'img'))
)

task('bower', () => {
    promise2.then(() => {
        gulpBower()
    })
})

task('init', () => {
    fs.symlink('stack/.sass-lint.yml', '../.sass-lint.yml', () => {})
    fs.symlink('stack/.eslintrc.js', '../.eslintrc.js', () => {})
})

task('build:css', () =>
    gulp.src([src.scss, '!' + src.scss + '/main.scss'], {
        cwd: src.prefix
    }, {
        base: '../src'
    })
    .pipe(gulpSass({
        outputStyle: 'compressed'
    }))
    .pipe(gulpPostCSS(pluginsProdBuild))
    .pipe(gulp.dest(src.public + 'css'))
    .pipe(gulpNotify({
        message: 'production build of css',
        title: 'Build Finished',
        icon: path.join(__dirname, 'stack.png'),
        onLast: true
    }))
)


task('copy:fonts', () => {
    gulp.src([
            src.prefix + src.fonts,
            src.bower + 'font-awesome/fonts/*.+(eot|svg|ttf|woff|woff2|otf)'
        ])
        .pipe(gulpNotify({
            message: 'Copied file: <%= file.relative %>!',
            title: 'Smidswater Stack',
            icon: path.join(__dirname, 'stack.png')
        }))
        .pipe(gulp.dest(src.public + 'fonts'))
})
task('lint:build:js:1', () =>
    gulp.src([src.prefix + src.js, '!' + src.prefix + src.js.substr(0, src.js.indexOf('*') - 1) + '/node_modules/**'])
    .pipe(gulpBetterRollup({
        'treeshake': true,
        'plugins': [
            rollupPluginNodeResolve({
                jsnext: true,
                main: true,
                browser: true
            }),
            rollupPluginCommonjs(),
            rollupPluginBabel({
                babelrc: false,
                presets: [
                    [
                        require.resolve('babel-preset-es2015'),
                        {
                            modules: false
                        }
                    ]
                ],
                plugins: [
                    require.resolve('babel-plugin-external-helpers')
                ],
                exclude: 'node_modules/**'
            })
        ]
    }, {
        format: 'iife'
    }))
    .pipe(gulp.dest(src.public + 'js'))
)

task('lint:build:js:2', () =>
    gulp.src([src.prefix + src.js, '!' + src.prefix + src.js.substr(0, src.js.indexOf('*') - 1) + '/node_modules/**'])
    .pipe(gulpBetterRollup({
        'treeshake': true,
        'plugins': [
            rollupPluginNodeResolve({
                jsnext: true,
                main: true,
                browser: true
            }),
            rollupPluginCommonjs(),
            rollupPluginBabel({
                babelrc: false,
                presets: [
                    [
                        require.resolve('babel-preset-es2015'),
                        {
                            modules: false
                        }
                    ]
                ],
                plugins: [
                    require.resolve('babel-plugin-external-helpers')
                ],
                exclude: 'node_modules/**'
            }),
            //rollupPluginUglify()
        ]
    }, {
        format: 'iife'
    }))
    .pipe(gulpRename({
        suffix: '.min'
    }))
    .pipe(gulpNotify({
        message: 'Build file: <%= file.relative %>!',
        title: 'Smidswater Stack',
        icon: path.join(__dirname, 'stack.png')
    }))
    .pipe(gulp.dest(src.public + 'js'))
)
task('build:js', () =>
    runSequence('lint:build:js:1', 'lint:build:js:2')
)

task('build', ['build:css', 'build:js', 'build:img', 'copy:fonts'])

task('watch:scss', () =>
    gulpWatch(src.prefix + src.scss, () =>
        gulp.start('build:css')
    )
)

task('watch:js', () =>
    gulpWatch(src.prefix + src.js, () => gulp.start('build:js'))
)

task('watch:img', () =>
    gulpWatch(src.prefix + src.img, () =>
        gulp.start('build:img')
    )
)

task('watch:fonts', () =>
    gulpWatch(src.prefix + src.fonts, () =>
        gulp.start('copy:fonts')
    )
)

task('watch', ['build', 'watch:scss', 'watch:js', 'watch:img', 'watch:fonts'])
task('default', ['build', 'watch'])
