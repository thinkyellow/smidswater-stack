module.exports = {
  "extends": "standard",
  "plugins": [
    "standard",
    "promise"
  ],
  "rules": {
    "no-console": ["error", {
      allow: ["warn", "error"]
    }],
    "guard-for-in": "error",
    "no-alert": "error",
    "no-debugger": "error",
    "arrow-body-style": ["error", "as-needed"],
    "no-lonely-if": "error"
  },
  "env": {
    "browser": true,
    "es6": true,
    "jquery": true
  }
};
